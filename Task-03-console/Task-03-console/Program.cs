﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please choose an option between 1-4");
            int result = Convert.ToInt32(Console.ReadLine());
            if(result == 1) {
                Console.WriteLine("1: Gummy Bear");
                Console.ReadKey();
            }else if(result == 2) {
                Console.WriteLine("2: Chocolate Bar");
                Console.ReadKey();
            }
            else if (result == 3) {
                Console.WriteLine("3: Fanta");
                Console.ReadKey();
            }
            else if (result == 4) {
                Console.WriteLine("4: Liquorice");
                Console.ReadKey();
            }
            else {
                Console.WriteLine("Sorry, that isn't a valid input");
                Console.ReadKey();
            }


                
            
        }
    }
}
