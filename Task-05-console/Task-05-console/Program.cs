﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {
            int playerNum = 0, randomNum = 0, points = 0;
            Random rnd = new Random();

            for (int i = 1; i <= 5; i++)
            {
                randomNum = rnd.Next(1, 6);
                Console.WriteLine("Please enter a number between 1-5");
                playerNum = Convert.ToInt32(Console.ReadLine());
                
                if (playerNum == randomNum)
                {
                    Console.WriteLine("You chose the right number!");
                    points++;
                }
                else
                {
                    Console.WriteLine("You didn't chose the right number");
                }
            }
            Console.WriteLine("Game Over!. You got {0} points!", points);
            Console.ReadKey();
        }
    }
}
