﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        int round = 1;
        int score = 0;
        public Form1()
        {
            InitializeComponent();
           
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            int randomNum = rnd.Next(1, 6);
            int playerNum = Convert.ToInt32(textBoxInput.Text);
            if (round < 5)
            {
                if (playerNum == randomNum)
                {
                    labelShow.Visible = true;
                    labelShow.Text = "Correct!";
                    round++;
                    score++;
                    textBoxInput.Clear();
                }
                else
                {
                    labelShow.Visible = true;
                    labelShow.Text = "Incorrect!"; 
                    textBoxInput.Clear();
                    round++;
                }
            }else {
                buttonSubmit.Enabled = false;
                labelFinal.Text = score.ToString();
                labelFinal.Visible = true;
                label3.Visible = true;

            }
        }
    }
}
