﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_wf
{
    public partial class Form1 : Form
    {
        List<string> dictionary = new List<string>();
        public Form1()
        
    {
            InitializeComponent();
            
            dictionary.Add("Peach");
            dictionary.Add("Apple");
            dictionary.Add("Banana");
            dictionary.Add("Tomato");
            dictionary.Add("Cucumber");

        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            
            bool result = dictionary.Contains(textBoxSearch.Text);
            label2.Text = Convert.ToString(result);
            if(result) {
                label1.Text = "Your word is in the dictionary";
            }else {
                label1.Text = "Your word is not in the dictionary";
            }
        }
    }
}
