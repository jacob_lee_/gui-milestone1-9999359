﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> dictionary = new List<string>();
            dictionary.Add("Peach");
            dictionary.Add("Apple");
            dictionary.Add("Banana");
            dictionary.Add("Tomato");
            dictionary.Add("Cucumber");

            Console.WriteLine("Search the Dictionary");
            string search = Console.ReadLine();
            bool result = dictionary.Contains(search);
            if(result) {
                Console.WriteLine("Your word is in the Dictionary");
            }else {
                Console.WriteLine("Your word is not in the Dictionary");
            }
            
            Console.ReadKey();
        }
    }
}
