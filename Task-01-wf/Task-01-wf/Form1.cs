﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonConKM_Click(object sender, EventArgs e)
        {
            textBoxKmOut.Text = Convert.ToString(Convert.ToDouble(textBoxKMIn.Text) * 0.62137119);
        }

        private void buttonConM_Click(object sender, EventArgs e)
        {
            textBoxMOut.Text = Convert.ToString(Convert.ToDouble(textBoxMIn.Text) * 1.609344);
        }
    }
}
