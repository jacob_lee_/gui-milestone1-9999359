﻿namespace Task_01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConM = new System.Windows.Forms.Button();
            this.buttonConKM = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Output = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxKmOut = new System.Windows.Forms.TextBox();
            this.textBoxMOut = new System.Windows.Forms.TextBox();
            this.textBoxMIn = new System.Windows.Forms.TextBox();
            this.textBoxKMIn = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonConM
            // 
            this.buttonConM.Location = new System.Drawing.Point(301, 265);
            this.buttonConM.Name = "buttonConM";
            this.buttonConM.Size = new System.Drawing.Size(75, 23);
            this.buttonConM.TabIndex = 0;
            this.buttonConM.Text = "Convert";
            this.buttonConM.UseVisualStyleBackColor = true;
            this.buttonConM.Click += new System.EventHandler(this.buttonConM_Click);
            // 
            // buttonConKM
            // 
            this.buttonConKM.Location = new System.Drawing.Point(76, 265);
            this.buttonConKM.Name = "buttonConKM";
            this.buttonConKM.Size = new System.Drawing.Size(75, 23);
            this.buttonConKM.TabIndex = 1;
            this.buttonConKM.Text = "Convert";
            this.buttonConKM.UseVisualStyleBackColor = true;
            this.buttonConKM.Click += new System.EventHandler(this.buttonConKM_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(315, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Output";
            // 
            // Output
            // 
            this.Output.AutoSize = true;
            this.Output.Location = new System.Drawing.Point(91, 201);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(39, 13);
            this.Output.TabIndex = 3;
            this.Output.Text = "Output";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(315, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "M to KM";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(91, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "KM to M";
            // 
            // textBoxKmOut
            // 
            this.textBoxKmOut.Enabled = false;
            this.textBoxKmOut.Location = new System.Drawing.Point(63, 217);
            this.textBoxKmOut.Name = "textBoxKmOut";
            this.textBoxKmOut.Size = new System.Drawing.Size(100, 20);
            this.textBoxKmOut.TabIndex = 6;
            // 
            // textBoxMOut
            // 
            this.textBoxMOut.Enabled = false;
            this.textBoxMOut.Location = new System.Drawing.Point(288, 217);
            this.textBoxMOut.Name = "textBoxMOut";
            this.textBoxMOut.Size = new System.Drawing.Size(100, 20);
            this.textBoxMOut.TabIndex = 7;
            // 
            // textBoxMIn
            // 
            this.textBoxMIn.Location = new System.Drawing.Point(288, 135);
            this.textBoxMIn.Name = "textBoxMIn";
            this.textBoxMIn.Size = new System.Drawing.Size(100, 20);
            this.textBoxMIn.TabIndex = 8;
            // 
            // textBoxKMIn
            // 
            this.textBoxKMIn.Location = new System.Drawing.Point(63, 135);
            this.textBoxKMIn.Name = "textBoxKMIn";
            this.textBoxKMIn.Size = new System.Drawing.Size(100, 20);
            this.textBoxKMIn.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 372);
            this.Controls.Add(this.textBoxKMIn);
            this.Controls.Add(this.textBoxMIn);
            this.Controls.Add(this.textBoxMOut);
            this.Controls.Add(this.textBoxKmOut);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonConKM);
            this.Controls.Add(this.buttonConM);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConM;
        private System.Windows.Forms.Button buttonConKM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Output;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxKmOut;
        private System.Windows.Forms.TextBox textBoxMOut;
        private System.Windows.Forms.TextBox textBoxMIn;
        private System.Windows.Forms.TextBox textBoxKMIn;
    }
}

