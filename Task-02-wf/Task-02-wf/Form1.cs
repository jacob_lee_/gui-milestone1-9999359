﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        int itemNum = 0;
        double total = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            total = total * 1.15;
            labelResult.Text = Convert.ToString(total);
        }

        private void buttonFinish_Click(object sender, EventArgs e)
        {
            total = total + Convert.ToDouble(textBoxPrices.Text);
            itemNum++;
            textBoxPrices.Text = "";
        }
    }
}
