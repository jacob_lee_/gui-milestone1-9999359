﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many items do you have?");
            int numItems = Convert.ToInt32(Console.ReadLine());
            double prices, total = 0;

            for(int i = 1; i <= numItems; i++) {
                Console.WriteLine("Please enter a price");
                prices = Convert.ToDouble(Console.ReadLine());
                total = total + prices;
            }
            total = total * 1.15;
            Console.WriteLine("Your total (including GST) is {0}", total);
            
            Console.ReadKey();
        }
    }
}
