﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        List<string> dictionary = new List<string>();
        public MainPage()
        {
            this.InitializeComponent();

            dictionary.Add("Peach");
            dictionary.Add("Apple");
            dictionary.Add("Banana");
            dictionary.Add("Tomato");
            dictionary.Add("Cucumber");
            
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            bool result = dictionary.Contains(textBoxSearch.Text);
            textBoxResult.Text = Convert.ToString(result);
            if (result)
            {
                textBoxResult.Text = "You word is in the dictionay";
            }
            else
            {
                textBoxResult.Text = "Your word is not in the dictionary";
            }
            }
        }
    }

