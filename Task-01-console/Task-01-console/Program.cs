﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Is your number in KM or M?");
            string choice = Console.ReadLine();
            double result, userNum;
            if(choice == "KM") {
                Console.WriteLine("Please enter your number");
                userNum = Convert.ToDouble(Console.ReadLine());
                result = userNum * 0.62137119;
               
                Console.WriteLine("Your number in miles is: {0}", result);

                Console.ReadKey();
            }
            else if(choice == "M") {
                Console.WriteLine("Please enter your number");
                userNum = Convert.ToDouble(Console.ReadLine());
                result = userNum * 1.609344;

                Console.WriteLine("Your number in kilometers is: {0}", result);

                Console.ReadKey();
            }
            else {
                Console.WriteLine("Your input is not valid");
            }
        }
    }
}
